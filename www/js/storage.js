class LStorage {
    get(item) {
        return JSON.parse(localStorage.getItem(item))
    }

    set(item, value) {
        localStorage.setItem(item, JSON.stringify(value));
        return value;
    }

    add(item, value) {
        let array = this.get(item);
        array.push(value);
        this.set(item, array);
    }

    remove(item, id) {
        let array = this.get(item);
        for (i=0; i < array.length; i++) {
            if (array[i].id == id) {
                array.splice(i, 1);
            }
        }
        this.set(item, array);
    }

    getView(item) {
        let result = this.get(item);
        
        if (!result) {
            result = this.set(item, []);
        }

        return result;
    }

    findById(item, id) {
        let array = this.get(item);
        for (let i=0; i < array.length; i++) {
            if (array[i].id == id) {
                return array[i];
            }
        }
        return null;
    }
}