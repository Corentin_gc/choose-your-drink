var $$ = Dom7;
var router = this;
var app = router.app;
var storage = new LStorage();

function getDrinksByName(name){
  $$('#result-drinks').empty()
  if(name !== ""){
    app.request.get(`https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${name}`).then((res) => {
      if(JSON.parse(res.data).drinks){
        JSON.parse(res.data).drinks.forEach(element => {
          $$('#result-drinks').append(
            `
              <li>
                <a href="/drink/${element.idDrink}/" class="item-content color-theme-white">
                  <div class="item-media">
                    <img src="${element.strDrinkThumb}" width="44" />
                  </div>
                  <div class="item-inner">
                    <div class="item-title-row">
                      <div class="item-title">${element.strDrink}</div>
                    </div>
                    <div class="item-subtitle">Glass type : ${element.strGlass}</div>
                  </div>
                </a>
              </li>
            `
          )
        });
      }else{
        $$('#result-drinks').append(
          `
            <li class="item-content">
              <div class="item-media">
              </div>
              <div class="item-inner">
                <div class="item-title-row">
                  <div class="item-title">No result found !</div>
                </div>
                <div class="item-subtitle"></div>
              </div>
            </li>
          `)
      }
    });
  }
}

async function getDrinkById(id) {
  return await app.request.get(`https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${id}`).then((res) => {
    let data = JSON.parse(res.data);
    let drink = data.drinks[0];

    let currentDrink = {
      id: drink.idDrink,
      name: drink.strDrink,
      image: drink.strDrinkThumb,
      glass: drink.strGlass,
      alcoholic: drink.strAlcoholic,
      category: drink.strCategory,
      instructions: drink.strInstructions,
      ingredients: []
    };

    for (i=1; i < 16; i++) {
      let ingredient = {
        name: drink[`strIngredient${i}`],
        measure: drink[`strMeasure${i}`]
      };

      if (ingredient.name == null || ingredient.name == "") {
        break;
      }

      currentDrink.ingredients.push(ingredient);
    }
    
    return currentDrink;
  });
}

function addNewFavorite(self) {
  let heart = self.srcElement.closest('svg');
  let data = heart.closest('.card-footer').dataset;

  // If the heart is empty (not favorite yet)
  if (heart.dataset.prefix == 'far') {
    let favorite = {
      id: data.id,
      name: data.name,
      image: data.image,
      glass: data.glass,
      alcoholic: data.alcoholic,
      category: data.category,
      instructions: data.instructions,
      ingredients: JSON.parse(data.ingredients)
    }

    storage.add('favorites', favorite);

    favoriteListAdd(favorite);
    favoriteOverviewAdd(favorite);
    toggleSuggestionLike(data.id, true);

    heart.dataset.prefix = 'fas'
  } else {
    // Remove the favorite
    storage.remove('favorites', data.id);

    favoriteListRemove(data.id);
    favoriteOverviewRemove(data.id);
    toggleSuggestionLike(data.id, false);

    heart.dataset.prefix = 'far'
  }
}

function favoriteListAdd(favorite) {
  let baseList = $$('#favorites-list').html();

  let newFav = `<li id="list-favorite-${favorite.id}">
    <a href="/drink/${favorite.id}/" class="item-link item-content">
      <div class="item-media">
        <img src="${favorite.image}" width="44" />
      </div>
      <div class="item-inner">
        <div class="item-title">
          <div class="item-header">${favorite.glass}</div>${favorite.name}
        </div>
        <div class="item-after"></div>
      </div>
    </a>
  </li>`;

  baseList += newFav;

  $$('#favorites-list').html(baseList);
}

function favoriteListRemove(id) {
  $$('#favorites-list').find(`#list-favorite-${id}`).remove();
}

function favoriteOverviewAdd(favorite) {
  // If there is already favorites
  if (!$$('#favorites-overview').html().includes('No favorite yet.')) {
    let baseList = $$('#favorites-overview').find('ul').html();

    let newFav = `<li class="item-content" id="overview-favorite-${favorite.id}">
      <div class="item-media">
        <img src="${favorite.image}" width="44" />
      </div>
      <div class="item-inner">
        <div class="item-title-row">
          <div class="item-title"><a href="/drink/${favorite.id}/">${favorite.name}</a></div>
        </div>
        <div class="item-subtitle">${favorite.glass}</div>
      </div>
    </li>`;

    baseList += newFav;

    $$('#favorites-overview').find('ul').html(baseList);
  } else {
    $$('#favorites-overview').html(`
      <ul>
        <li class="item-content" id="overview-favorite-${favorite.id}">
          <div class="item-media">
            <img src="${favorite.image}" width="44" />
          </div>
          <div class="item-inner">
            <div class="item-title-row">
              <div class="item-title"><a href="/drink/${favorite.id}/">${favorite.name}</a></div>
            </div>
            <div class="item-subtitle">${favorite.glass}</div>
          </div>
        </li>
      </ul>
    `);
  }

}

function favoriteOverviewRemove(id) {
  $$('#favorites-overview').find(`#overview-favorite-${id}`).remove();

  // If the last favorite is removed we put back the "no favorites" text
  if (!$$('#favorites-overview').html().includes('li')) {
    $$('#favorites-overview').html('<p class="padding-left">No favorite yet.</p>');
  }
}

function toggleSuggestionLike(id, state) {
  if ($$('#suggestion-data').data('id') == id) {
    if (state) {
      $$('#suggestion-fav').removeClass('fa-regular');
      $$('#suggestion-fav').addClass('fa-solid');

      // If the drink is opened in the drinks menu or in the favorites list
      if ($$(`#drink-data-${id}`)[0]) {
        $$(`#drink-fav-${id}`).removeClass('fa-regular');
        $$(`#drink-fav-${id}`).addClass('fa-solid');
      }
    } else {
      $$('#suggestion-fav').removeClass('fa-solid');
      $$('#suggestion-fav').addClass('fa-regular');

      // If the drink is opened in the drinks menu or in the favorites list
      if ($$(`#drink-data-${id}`)[0]) {
        $$(`#drink-fav-${id}`).removeClass('fa-solid');
        $$(`#drink-fav-${id}`).addClass('fa-regular');
      }
    }
  }
}