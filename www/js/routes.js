var $$ = Dom7;

var routes = [
  {
    path: '/',
    url: './index.html',
  },
  {
    path: '/bar/',
    componentUrl: './pages/bar.html'
  },
  {
    path: '/drinks/',
    url: './pages/drinks.html',
    on: {
      pageAfterIn: function (e, page) {
        var router = this;
        var app = router.app;
        var searchbar = app.searchbar.create({
          customSearch: true,
          el: '.searchbar',
          on: {
            search(sb, query, previousQuery) {
              getDrinksByName(query)
            }
          }
        });
      },
    }
  },
  {
    path: '/favorites/',
    componentUrl: './pages/favorites.html',
  },
  {
    path: '/drink/:id/',
    componentUrl: './pages/drink.html'
  },

  // Default route (404 page). MUST BE THE LAST
  {
    path: '(.*)',
    url: './pages/404.html',
  },
];
